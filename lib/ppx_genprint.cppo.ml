open Ppxlib
(* open Caml *)

(*
extract from compiler call, add to tuple argument to 'print', use to find the cmt file.
        lid "include_dirs", make_list make_string !Clflags.include_dirs;
        lid "load_path",    make_list make_string (Load_path.get_paths ());

the cmt for current file with be stored alongside the object.
the load-path should be byte/native for dune.
difference with include-dirs? load-path includes 'include-dirs under 4.08
x
*)

let envar_set name = try ignore@@Sys.getenv name;true with _->false
let remove_extensions = envar_set "GENPRINT_REMOVE"

let ctxt_lbl loc=
  let open Ast_builder.Default in
  let src = loc.loc_start.pos_fname in
  let hsrc= Hashtbl.hash src in
  "__context_"^(string_of_int hsrc)

let count = ref 0
let cwd = Sys.getcwd()
let fullpath f=
  if Filename.is_relative f then
    Filename.concat cwd f
  else
    f

let getwords l =
  List.fold_left (fun msg (_,word) ->
      match word.pexp_desc with
      | Pexp_ident id ->
         let word'= Longident.name id.txt in
         (word' ^" "^ msg)
      | Pexp_construct({txt=Lident lid} ,_)->
         (lid ^" "^ msg)
      (* | Pexp_constant _-> *)
      | _ ->
         Location.raise_errorf ~loc:word.pexp_loc "this should be a word")
    "" l

let set_loadpath locopt =
  let loc = match locopt with
    | None-> Location.none
    | Some loc->loc
  in
(* Printf.printf" FILE? %b %s\n" (locopt<>None) loc.loc_start.pos_fname; *)
  (* should be setup from a ppx.context *)
(*
ocaml/parsing/ast_mapper.ml:
  let make ~tool_name () =
    let fields =
      [
        lid "tool_name",    make_string tool_name;
        lid "include_dirs", make_list make_string !Clflags.include_dirs;
        lid "load_path",    make_list make_string !Config.load_path;
        lid "open_modules", make_list make_string !Clflags.open_modules;
        lid "for_package",  make_option make_string !Clflags.for_package;
        lid "debug",        make_bool !Clflags.debug;
        lid "use_threads",  make_bool !Clflags.use_threads;
        lid "use_vmthreads", make_bool !Clflags.use_vmthreads;
        get_cookies ()

and from 4.08:
        lid "recursive_types", make_bool !Clflags.recursive_types;
        lid "principal", make_bool !Clflags.principal;
        lid "transparent_modules", make_bool !Clflags.transparent_modules;
        lid "unboxed_types", make_bool !Clflags.unboxed_types;
        lid "unsafe_string", make_bool !Clflags.unsafe_string;

*)
#if OCAML_VERSION < (4,08,0)
  let lp = !Ocaml_common.Config.load_path in
#else
  let lp = Ocaml_common.Load_path.get_paths() in
#endif
  let lp = List.map fullpath lp in

  (* workaround for possible modname<>file basename under dune *)
  let openmods= !Ocaml_common.Clflags.open_modules in
  let dune = envar_set "INSIDE_DUNE" in
  let open Ast_builder.Default in
  let dune_label = Located.mk ~loc (Longident.parse "Genprint__Genprintaux.dune") in
  let openmods_label = Located.mk ~loc (Longident.parse "Genprint__Genprintaux.open_modules") in

  let flags_label = Located.mk ~loc (Longident.parse "Genprint__Genprintaux.compile_flags") in
  let loadpath_label = Located.mk ~loc (Longident.parse "Genprint__Genprintaux.loadpath") in

  let flagrefs=
    Ocaml_common.Clflags.[|
      recursive_types;
      principal;
      transparent_modules;
      unboxed_types;
      unsafe_string;
    |] in
  let flags=  Array.map (fun r-> !r) flagrefs in

  let src = loc.loc_start.pos_fname in
  let src_label = Located.mk ~loc (Longident.parse "Genprint__Genprintaux.source") in

  let exp = [%expr [%e Ast_builder.Default.(
    pexp_record ~loc [
      (src_label, estring ~loc @@ Filename.basename src);
      (dune_label, ebool ~loc dune);
      (openmods_label, elist ~loc (List.map(estring ~loc) openmods));
      (loadpath_label, elist ~loc (List.map (estring ~loc) lp));
      (flags_label,pexp_array ~loc (Array.to_list(Array.map (ebool ~loc) flags)));
    ] None);
  ]] in
  let ctxt_lbl= Location.{loc; txt=ctxt_lbl loc} in
  let si= pstr_value ~loc Asttypes.Nonrecursive [
              value_binding ~loc ~pat:(ppat_var ~loc ctxt_lbl) ~expr:exp ] in
(*
               [{vb_pat = {pat_desc=Tpat_var(id,_)}}
               ]
ctxt_lbl [%e exp] in
*)
  (* [ [%stri let [%e ctxt_lbl] = [%e exp]]], [] *)
  [si], []

(* put this value into every compiled unit once *)
let () =
  Driver.register_transformation
    ~enclose_impl:set_loadpath
    "genprint-context" ~extensions:[]

(* cache mode can be detected by examining data as loaded
(* used internally *)
val ppx_mode: bool ref

let set_ppxmode locopt =
  let loc = match locopt with
    | None-> Location.none
    | Some loc->loc
  in
  [[%stri let _ = Genprint.ppx_mode:=true ]], []

(* put this value into every compiled unit once *)
let () =
  Driver.register_transformation
    ~enclose_impl:set_ppxmode
    "genprint-ppxmode" ~extensions:[]
*)


let expand ~loc ~path (e : expression) =
  incr count;

  (* seemingly a bug in ppxlib that is now slapping .Map on  *)
  let path = if Filename.check_suffix path ".Map" 
                ||  Filename.check_suffix path ".Make" then
               Filename.remove_extension path
             else path in

  (*free-form text before the value including capitalisation *)
  let words, e =
    match e.pexp_desc with
    |Pexp_apply(e,el)->
      begin
        match List.rev el with
        | [] -> assert false          (* would not be an apply without something *)
        | (_,v)::tl ->                (* the last arg is the expression to eval *)
           let msg = getwords @@ tl@[Nolabel,e] in
          msg,v
      end
    | _-> "",e
  in
  if remove_extensions then [%expr let _=[%e e] in ()] else
  [%expr [%e Ast_builder.Default.(
   pexp_apply ~loc (
    pexp_apply ~loc
      (pexp_ident ~loc (Located.mk ~loc (Longident.parse "Genprint.print") ))
      [Nolabel,estring ~loc words;
       Nolabel,pexp_tuple ~loc [ 
                   (eint ~loc !count);
                   (estring ~loc @@ Filename.basename path);
                   (pexp_ident ~loc (
                        Located.mk ~loc (Longident.parse (ctxt_lbl loc)) ));
                 ]
      ])
    [Nolabel,e]
  )]]


let genprint =
  Extension.declare
    "pr"
    Extension.Context.expression
    Ast_pattern.(pstr ((pstr_eval __ nil) ^:: nil))
    expand


let () =
  Driver.register_transformation
    "genprint" ~extensions:[ genprint ]

let expand ~loc ~path (payload : payload) = 
  incr count;
  (* let open Ast_builder.Default in *)
  let words = match payload with
    |PStr [] -> ""
    |PStr [{pstr_desc=Pstr_eval(e,_)}]->
      begin match e.pexp_desc with
      |Pexp_apply(e,el)-> getwords @@ (List.rev el) @[Nolabel,e]
      | _-> getwords [Nolabel,e]
      end
    | _ -> Location.raise_errorf ~loc "improper syntax: empty or non-keyword word and Words"
  in
  if remove_extensions then [%expr fun x->x] else
  [%expr [%e Ast_builder.Default.(
    pexp_apply ~loc
      (pexp_ident ~loc (Located.mk ~loc (Longident.parse "Genprint.print_with_return") ))
      [Nolabel,estring ~loc words;
       Nolabel,pexp_tuple ~loc [
                   (eint ~loc !count);
                   (estring ~loc @@ Filename.basename path);
                   (pexp_ident ~loc (
                        Located.mk ~loc (Longident.parse (ctxt_lbl loc)) ));
                 ]
      ]
  )]]

let genprint =
  Extension.declare
    "prr"
    Extension.Context.expression
    (* Ast_pattern.(pstr ((pstr_eval __ nil) ^:: nil)) *)
    Ast_pattern.( (( __ ) ))
    expand


let () =
  Driver.register_transformation
    "genprint-with-return" ~extensions:[ genprint ]

(* [%prs nice to print escapes {x} and {y}]; *)
type s = S of string | E of string

let fold s =
  let rec fold_open pos acc =
    try
      let bpos = String.index_from s pos '{' in
      let pre = String.sub s pos (bpos-pos) in
      let acc = fold_close (bpos+1) acc in
      S pre :: acc
    with Not_found->
      let rem = String.sub s pos (String.length s -pos) in
      S rem :: acc
  and fold_close pos acc =
    try
      let bpos = String.index_from s pos '}' in
      let post = String.sub s pos (bpos-pos) in
      let acc = fold_open (bpos+1) acc in
      E post :: acc
    with Not_found->raise Exit
  in try fold_open 0 [] with Exit -> []

let custom_parse ~loc ~path s=
  let l=fold s in
  (* convert to a sequence of print statements *)
  let open Ast_builder.Default in
  let rec conv ~loc =function
    | []->[]
    | S s :: E exp :: tl ->
      if exp="" then
        Location.raise_errorf ~loc "empty expression not meaningful";
      let lexbuf = Lexing.from_string exp in
      let pt = try
          Parse.expression lexbuf
        with _->
          Location.raise_errorf ~loc "syntax error"
      in
      let e =pexp_apply ~loc (
          pexp_apply ~loc
            (pexp_ident ~loc (Located.mk ~loc (Longident.parse "Genprint.print_inline") ))
            [Nolabel,estring ~loc s;
             Nolabel,pexp_tuple ~loc [ 
               (eint ~loc !count);
               (estring ~loc @@ Filename.basename path);
               (pexp_ident ~loc (
                    Located.mk ~loc (Longident.parse (ctxt_lbl loc)) ));
             ]
            ])
          [Nolabel,pt]
      in
      incr count;               (* each printable arg needs its own *)
      e :: conv ~loc tl
    | S s ::_->
      (* trailing or without any inline {} *)
      let e =
        pexp_apply ~loc
          (pexp_ident ~loc (Located.mk ~loc (Longident.parse "Genprint.print_inline_end") ))
          [Nolabel,estring ~loc s]
      in [e]
    | E s ::_->
      Location.raise_errorf ~loc "unexpected format error!???"
  in
  List.rev@@ conv ~loc l

let expand ~loc ~path (e : expression) =
  let loc = e.pexp_loc in
  incr count;
  let s = match e with
#if OCAML_VERSION >= (4,11,0)
    |{pexp_desc=Pexp_constant(Pconst_string(s,_,_)) }->s
#else
    |{pexp_desc=Pexp_constant(Pconst_string(s,_)) }->s
#endif
    |_->
      Location.raise_errorf ~loc "this should be a string only with embedded OCaml terms"
  in
  if remove_extensions then [%expr let _=[%e e] in ()] else
  [%expr [%e Ast_builder.Default.(
   match custom_parse ~loc ~path s with
     | []->
       Location.raise_errorf ~loc "Forgotten closing brace?"
     | [e]-> e
     | e :: es ->
       List.fold_left (fun seq e-> pexp_sequence ~loc e seq) e es
  )]]


let genprint =
  Extension.declare
    "prs"
    Extension.Context.expression
    Ast_pattern.(pstr ((pstr_eval __ nil) ^:: nil))
    expand


let () =
  Driver.register_transformation
    "genprint-inline" ~extensions:[ genprint ]

(* custom printer installation *)
let install_printer ~loc ~path (fn : expression) = 
  incr count;
  if remove_extensions then [%expr let _=[%e fn] in ()] else
  [%expr [%e Ast_builder.Default.(
    pexp_apply ~loc
      (pexp_ident ~loc (Located.mk ~loc (Longident.parse "Genprint.install_printer") ))
      [Nolabel,fn;
       Nolabel,pexp_tuple ~loc [
                   (eint ~loc !count);
                   (estring ~loc @@ Filename.basename path);
                   (pexp_ident ~loc (
                        Located.mk ~loc (Longident.parse (ctxt_lbl loc)) ));
                 ]
      ]
  )]]

let genprint =
  Extension.declare
    "install_printer"
    Extension.Context.expression
    Ast_pattern.(pstr ((pstr_eval __ nil) ^:: nil))
    install_printer


let () =
  Driver.register_transformation
    "genprint-install-printer" ~extensions:[ genprint ]

let remove_printer ~loc ~path (fn : expression) = 
  incr count;
  if remove_extensions then [%expr let _=[%e fn] in ()] else

  [%expr [%e Ast_builder.Default.(
    pexp_apply ~loc
      (pexp_ident ~loc (Located.mk ~loc (Longident.parse "Genprint.remove_printer") ))
      [Nolabel,fn;
       Nolabel,pexp_tuple ~loc [
                   (eint ~loc !count);
                   (estring ~loc @@ Filename.basename path);
                   (pexp_ident ~loc (
                        Located.mk ~loc (Longident.parse (ctxt_lbl loc)) ));
                 ]
      ]
  )]]

let genprint =
  Extension.declare
    "remove_printer"
    Extension.Context.expression
    Ast_pattern.(pstr ((pstr_eval __ nil) ^:: nil))
    remove_printer


let () =
  Driver.register_transformation
    "genprint-remove-printer" ~extensions:[ genprint ]


(*
let expand_ff ~loc ~path (lid : longident) (el : (arg_label * expression) list) =
...

let genprint_freeform =
    Extension.declare
      "prs"
      Extension.Context.expression
      (* pattern appearing in [%pr <sss> v] is the application of the ident to the value *)
      Ast_pattern.(pstr ((pstr_eval (pexp_apply (pexp_ident __) ( __)  ) nil ) ^:: nil))
      expand_ff
*)
