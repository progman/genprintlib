val cmtpath: string -> unit
val all_libs_opaque: bool -> unit
val max_printer_depth : int ref
val max_printer_steps : int ref
val formatter : Format.formatter ref
val flush_at_once : bool ref
val prompt: string -> unit

(* cannot abstract the context info as construction embeddded
   within each user file, so as a compromise it's put
   here so the Genprint intf only shows the standard name for
   an abstract type, t.
 *)
type context = {
  source: string;
  loadpath: string list;
  compile_flags: bool array;
  (* the path and suffixless basename of a module's build artefacts.
     under Dune et el, not beside the src and often named differently from the
     straight module name. *)
  (* outputprefix: string; *)
  (* a workaround for the outputprefix not being part of the ppx context *)
  dune: bool;
  open_modules: string list;
}

(* the nth %pr in file, the file, the ppx context *)
type t = int * string * context

val print: string -> t -> 'a -> unit
val print_inline: string -> t -> 'a -> unit
val print_inline_end: string -> unit
val print_with_return: string -> t -> 'a -> 'a

type 'a printer_type = Format.formatter -> 'a -> unit

val install_printer: 'a printer_type -> t -> unit
val remove_printer: 'a printer_type -> t -> unit

(* ocamldebug support *)
(*open Format
module LocalPrinter :
  sig
    type t
    val install_printer :
          Path.t -> Types.type_expr -> (formatter -> t -> unit) -> unit

    val remove_printer : Path.t -> unit
    val outval_of_untyped_exception : t -> Outcometree.out_value
    val outval_of_value :
          int -> int ->
          (int -> t -> Types.type_expr -> Outcometree.out_value option) ->
          Env.t -> t -> Types.type_expr -> Outcometree.out_value
  end
*)
val unabstract_type:
  Path.t -> Env.t -> (Env.t -> Outcometree.out_value) -> Outcometree.out_value

val debugger_unabstract_global: Path.t -> context option -> Env.t -> Env.t
val debugger_unabstract_local: Location.t -> string -> context option -> Env.t -> Env.t
(* toplevel phrase processing *)
val scan_phrase: Typedtree.structure -> unit
val filter_phrase: Typedtree.structure_item list -> Typedtree.structure_item list

val refresh: unit->unit
