(* public section *)
val cmtpath: string -> unit
(* whether to disallow recursive examination of non-ocaml libraries' cmt files.
   default: false=disallow but results in slower printing  *)
val all_libs_opaque: bool -> unit

(* depth/steps initialised as per Toploop versions *)
val max_printer_depth : int ref
val max_printer_steps : int ref
(* initialised to std_formatter *)
val formatter : Format.formatter ref
(* whether flush after each message. default: true *)
val flush_at_once : bool ref

(* alter the default '=>' prompt *)
val prompt: string -> unit

(* Non-public section but required *)
type t = Genprintaux.t

(* these functions cannot be called other than through the ppx extension. *)
val print: string -> t -> 'a -> unit
val print_inline: string -> t -> 'a -> unit
val print_inline_end: string -> unit
val print_with_return: string -> t -> 'a -> 'a

type 'a printer_type = Format.formatter -> 'a -> unit

val install_printer: 'a printer_type -> t -> unit
val remove_printer: 'a printer_type -> t -> unit

val refresh: unit->unit
