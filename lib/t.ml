type s = S of string | E of string

let fold s =
  let rec fold_open pos acc =
    try
      let bpos = String.index_from s pos '{' in
      let pre = String.sub s pos (bpos-pos) in
      let acc = fold_close (bpos+1) acc in
      S pre :: acc
    with Not_found->
      let rem = String.sub s pos (String.length s -pos) in
      S rem :: acc
  and fold_close pos acc =
    try
      let bpos = String.index_from s pos '}' in
      let post = String.sub s pos (bpos-pos) in
      let acc = fold_open (bpos+1) acc in
      E post :: acc
    with Not_found->[]
  in fold_open 0 []  
;;

(* fold "ab cd {efg} hi {jk} lmn op" *) 
