(* This file is free software. See file "license" for more details. *)

(* [@@@warning "-26-27-34-39-16"] *)

let debug=false

let normalize_path env p=
#if OCAML_VERSION < (4,08,0)
  Env.normalize_path None env p
#else
  Env.normalize_module_path None env p
#endif


open Typedtree

(* compilation info garnered via ppx context from compiler for 
embedding in each srcfile.
cannot abstract the context info as construction embeddded
within each user file, so as a compromise it's put
there so the Genprint intf only exposes the standard name for
an abstract type, t.
 *)
type context = {
  source: string;
  loadpath: string list;
  compile_flags: bool array;
  (* the path and suffixless basename of a module's build artefacts.
     under Dune et el, not beside the src and often named differently from the
     straight module name. *)
  (* outputprefix: string; *)

  (* a workaround for the outputprefix not being part of the ppx context *)
  dune: bool;
  open_modules: string list;
}

(* the nth %pr in file, the file, the ppx context *)
type t = int * string * context

(* the partial typechecking inside will otherwise repeat compiler warnings so turned off *)
let _=
  Warnings.parse_options false "-a"

(* DEFUNCT. cmt directories had from ppx.context passed in by compiler *)
(* actually not quite - if user must use (preprocess (pps genprint.ppx ...)) and give up
   auto locating of cmts, then Genprint.cmtpath can be used to be explicit.
*)

(* allow for specifying recursive search on a directory *)
let rec expand acc name=
  if Sys.is_directory name then
    let content = Array.map (fun f -> Filename.concat name f) @@ Sys.readdir name in
    name :: (Array.fold_left expand [] content) @ acc
  else
    acc

let expand (l: string list) : string list =
  let excludes = ref [] in
  let mk_unrec acc h =
    let h = String.trim h in
    let hl=String.length h in
    if hl>1 && (h.[0]='r' || h.[0]='R') && h.[1]=' ' then
      let dirs = expand [] @@ String.sub h 2 (hl - 2) in
      (* let _=print_endline @@ "rec "^ (String.sub h 2 (String.length h - 2)) in *)
      dirs @ acc
    else
    if hl>1 && (h.[0]='x' || h.[0]='X') && h.[1]=' ' then(
      excludes := String.sub h 2 (hl - 2)  :: !excludes;
      acc)
    else
      if hl>0 then h :: acc else acc
  in
  (* a/b/c - a/b *)
  let exclude prefix d =
    String.length prefix > String.length d ||
    String.sub d 0 (String.length prefix) <> prefix in
  let expanded = List.fold_left mk_unrec [] l in
  List.fold_left (fun acc x -> List.filter (exclude x) acc) expanded !excludes

let extra_dirs=ref []
(* cmi locations are cmtpath + rec stdlib *)
(*
let set_loadpath dirs =
  let ds = dirs @ !extra_dirs in
#if OCAML_VERSION < (4,08,0)
  Config.load_path := ds 
(* @  expand [] Config.standard_library @ !Config.load_path *)
#else
  Load_path.init ds
(* @  expand [] Config.standard_library *)
#endif
*)

let set_loadpath dirs f=
  let ds = dirs @ !extra_dirs in
(* List.iteri (fun i p-> Printf.printf "SET LOADPATH [%d] %s\n%!" i p) ds; *)
#if OCAML_VERSION < (4,08,0)
  Misc.(protect_refs [R (Config.load_path, ds)]) f
#else
  let saved=Load_path.get_paths() in
  Load_path.init ds;
  Misc.try_finally ~always:(fun _->Load_path.init saved) f
#endif

(* wrong to have changed to this - recreating loadpath as used by compiler on
   a module in which appeared one or more print stmts. this accuracy allows
   for correctly locating the cmi and therefore the cmt if it exists.
   to prevent interference with code also making use of the compiler-libs
   the current loadpath has to be saved then restored once the print stmt has
   resolved and the cache reflecting any new cmt info. 

#if OCAML_VERSION < (4,08,0)
module SSet = Misc.StringSet
#else
module SSet = Misc.Stdlib.String.Set
#endif
let addl s l = SSet.(fold add s (of_list l))

let set_loadpath dirs =
#if OCAML_VERSION < (4,08,0)
  let ds = dirs @ !extra_dirs @ !Config.load_path in
  let s = addl(addl(addl SSet.empty ds) dirs) !extra_dirs in
  let l = SSet.fold (fun h t-> h::t) s [] in
  Config.load_path := l
(* @  expand [] Config.standard_library @ !Config.load_path *)
#else
  let ds = Load_path.get_paths() in
  let s = addl(addl(addl SSet.empty ds) dirs) !extra_dirs in
  Load_path.init [];
  SSet.iter Load_path.add_dir s
(* ;List.iteri (fun i p-> Printf.printf "SET LOADPATH [%d] %s\n" i p) (Load_path.get_paths()) *)
(* @  expand [] Config.standard_library *)
#endif
*)

let get_loadpath () = 
#if OCAML_VERSION < (4,08,0)
  !Config.load_path
#else
  Load_path.get_paths()
#endif

(* for user to set additional dirs *)
(* ... just set extras and allow the oft called set-loadpath to incorporate it *)
let cmtpath l= 
  try
    let dirs = String.split_on_char ':' l |> expand in
    extra_dirs:= dirs @ [Config.standard_library];
  with exn ->
    failwith("Genprint: incorrect cmtpath format: "^Printexc.to_string exn)

let _=
  try
    let path= Sys.getenv "GENPRINT_CMTPATH" in
    cmtpath path
  with _->()

let envar_set name = try ignore@@Sys.getenv name;true with _->false
let debug= envar_set "GENPRINT_DEBUG"

(* if cmt file no longer exists but 'robust' set then
   add cache entry anyway. And when a find_cmt fails see if there
   is already a cache entry for the module and return that entry.
   dune has the odd behaviour of deleting .cmt files on repeated
   execs and this is to get round that without resorting to
   making spurious effort to force dune to recreate.
 *)
let robust=envar_set "GENPRINT_ROBUST"


(* capture the special compilation flags via ppx context *)
let flagrefs=
  Clflags.[|
    recursive_types;
    principal;
    transparent_modules;
    unboxed_types;
    unsafe_string;
  |]

(* a string of special purpose ocaml flags that may or may not get passed in through
   the compiler's ppx context. *)
let set_flags fs =
  Array.iter2 (fun r b-> r:=b) flagrefs fs;
  try 
    let fs = Sys.getenv "GENPRINT_FLAGS" in
    let fs = List.map String.trim (String.split_on_char ' ' fs) in
    let doflag=Clflags.(function
      | "-rectypes"->recursive_types:=true
      | "-no-rectypes"->recursive_types:=false
      | "-principal"->principal:=true
      | "-no-principal"->principal:=false
      | "-alias-deps"->transparent_modules:=true
      | "-no-alias-deps"->transparent_modules:=false
      | "-unboxed-types"->unboxed_types:=true
      | "-no-unboxed-types"->unboxed_types:=false
      | "-safe-string"->unsafe_string:=false
      | "-unsafe-string"->unsafe_string:=true
      | s-> failwith("Genprint: unknown flag in GENPRINT_FLAGS: "^s))
    in
    List.iter doflag fs
  with Not_found-> ()

let get_flags ()= Array.map (fun r-> !r) flagrefs

(* whether to report inability to unabstract due to Envaux.env-of-only-summary not able
to find a cmi. otherwise silently leave unabstracted *)
let ignore_missing = ref @@envar_set "GENPRINT_IGNORE_MISSING"

(* when stdlib-restricted is true that is to say only ocaml libs are regarded as opaque *)
let stdlib_restricted = ref @@ not@@envar_set "GENPRINT_INSTALLED_LIBS_OPAQUE"
let all_libs_opaque b= stdlib_restricted:= not b

let excepted file=
  let libmods_with_submod=["hashtbl"; "ephemeron";"stdLabels";"moreLabels"] in
  (* let libmods_with_submod=["ephemeron";Filename.concat "compiler-libs" "misc"] in *)
(*
#if OCAML_VERSION >= (4,10,0)
  let libmods_with_submod="sys" :: libmods_with_submod in
#endif
*)
  List.exists (fun m ->
#if OCAML_VERSION < (4,07,0)
      file = Filename.concat Config.standard_library (m ^".cmt"))
#else
#if OCAML_VERSION < (4,14,0)
#else
      let m= String.capitalize_ascii m in
#endif
      file = Filename.concat Config.standard_library ("stdlib__"^ m ^".cmt"))
  (* || file = Filename.concat Config.standard_library (m ^".cmt")) *)
#endif
    libmods_with_submod

let compiler_libs = Filename.concat Config.standard_library "compiler-libs"
let has_prefix filepath prefix=
  try
    prefix = String.sub (Filename.dirname filepath) 0 (String.length prefix)
  with Invalid_argument _-> false

let is_restricted file=
  (* the problem with taking one step up from the stdlib to capture other installed libs
     is if some module defines a submodule with a sig constraint applied inside the src -
     it will not be unabstracted. *)
  let lib_root =
    if !stdlib_restricted then Config.standard_library else
    Filename.dirname Config.standard_library in
  (* to indicate whether module should be regarded as a not-to-be-typechecked for abstracted
     sub-modules. this doesn't stop mli/cmi being discounted. *)
  let regard_as_opaque=
    has_prefix file lib_root
    && not@@excepted file
    (* and when allowing expanded library definition, it is not a compiler file...  *)
    && not (!stdlib_restricted && has_prefix file compiler_libs)
  in
  (* Printf.printf "RESTRICTED? %s %b restrict=%b prefixed=%b haslibroot=%b\n%!" file regard_as_opaque !stdlib_restricted (has_prefix file compiler_libs) (has_prefix file lib_root); *)
  regard_as_opaque

(* to distinguish between inline print statement use and ocamldebug *)
(* - debugger requires all tt structures to be available for location/scope searching
   and an effort is made to minimise ppx cache size by excluding tt trees though a
   debugger generated cache is fine for the ppx code to use. *)
(* let ppx_mode = ref true *)

(* save cache to disk. computed signatures are expensive and could encompass all
   modules of a project plus its libraries.*)
let cachefile=".genprint.cache" 

(* the count doubles as a discriminator and time-of-addition  *)
let cache_count=ref 0

type globalsig={
    gs_modname: string;
    gs_sig: Types.signature_item;
    gs_cmtfile: string;
    gs_timestamp: float;
    gs_unique_id: int;          (* gives order of addition *)
    gs_loadpath: string list;
    gs_flags: bool array;
    gs_structure: Typedtree.structure(* option*);
    gs_valid: bool;             (* since whole files are processed and partial success may be
                                   enough for the intended printing, this flag allows identifying
                                   the failed parts and eliminating them on next load. *)
  }

(* signatures derived from unabstracted parsetrees *)
let sig_cache = Hashtbl.create 5 
(* recording of print calling sites to enable matching typedtree type info  *)
let pr_cache =  Hashtbl.create 5

(* when cache was previously populated by ppx print prompted sigs, without structures,
   and debugger now needs them. Saves some resource.
*)
let empty_cache()=
  Hashtbl.clear pr_cache;
  Hashtbl.clear sig_cache;
  cache_count:=0

(* loading of cache necessitates re-processing of out-of-date cmt's *)
let process_cmt_fwd = ref (fun _ -> assert false) (* set before load_cache runs *)


let add_pr x y=
  Hashtbl.replace pr_cache x y

let find_pr = Hashtbl.find pr_cache
let find_globalsig x = 
  let r=
    Hashtbl.find sig_cache x
  in
  (* print_endline@@"FOUND GSIG: "^x; *)
(*
Format.(
  printf "@[---SIGGY---@\n";
  Printtyp.signature std_formatter [r.gs_sig];
  printf "@\n---@]@\n";
);
*)
  r

let try_finally ~always f =
  match f () with
  | x ->
    always ();
    x
  | exception e ->
    always ();
    raise e


let dirty_cache=ref false

let maybe_add_sig gsig=
  ignore@@ try
    find_globalsig gsig.gs_cmtfile
  with Not_found->
    (* Printf.printf"REGEN for %s %s\n%!" gsig.gs_modname gsig.gs_cmtfile; *)
    (* set the compilation environment as prevailed previously *)
    set_loadpath gsig.gs_loadpath (fun _->
        set_flags gsig.gs_flags;
        !process_cmt_fwd gsig.gs_modname gsig.gs_cmtfile)

let add_sig= Hashtbl.add sig_cache

(*
revised cmt gets loaded: regen needed
assigned new id
what if other cmts depended on it?
purpose of id is to proc-cmt in right order so as not
to proc unnecessarily. ao suggesting one can find/preserve the order 
after updates... 
there is only contention between regens as to order
A before B but A depends on B means B gets re-pro'd 1st.
when B's turn for regen there is no need for it cos already in scache 
so instead of proc-cmt use find-gsig? just need to 
any benefit to a uid? 
*)

let load_cache()=
  try
    let ch=open_in_bin cachefile in
   try_finally
     ~always:(fun () -> close_in ch)
     (fun()->
        let (ver, restricted) = input_value ch in
        if ver <> Config.version
         || restricted <> !stdlib_restricted
        then raise Not_found;
        let (scache,tcache) : (_ Hashtbl.t * _) =
          Marshal.from_channel ch in
        (* cache_count:=count; *)
        (* needs a correct loadpath before running env-of-only-summary. so not using it! *)
        Hashtbl.iter (fun k (_r, p,ty,env) ->
            (* nb. r for ready may still be false if run of program never actually
               encountered a particular print stmt *)
            Hashtbl.add pr_cache k (ref false, p,ty, (*Envaux.env_of_only_summary*) env)) tcache;
        let all = ref [] in
        (* filter cache of not-fully-processed sigs to allow for a re-attempt *)
        Hashtbl.iter (fun k gs-> if gs.gs_valid then all := (k,gs):: !all
(* else Printf.printf "INVALID %s\n" gs.gs_cmtfile *)
          ) scache;

        let regen,noregen=
          List.partition
            (fun (k, gsig) ->
            (* Printf.printf"REGEN? for %s %f/%f\n%!" gsig.gs_cmtfile (Unix.stat gsig.gs_cmtfile).st_mtime  gsig.gs_timestamp; *)
            Sys.file_exists gsig.gs_cmtfile
               && (Unix.stat gsig.gs_cmtfile).st_mtime > gsig.gs_timestamp)
            !all
        in
        List.iter (fun (k,gsig)-> add_sig k gsig) noregen;
        (* with all unchanged sigs in the cache the order of re-processing the
           changed no longer matters as long as there's a check for cache presence 
           to avoid duplication of effort.
         *)
        List.iter (fun (_k,gsig)-> maybe_add_sig gsig) regen;


(*
        (* to avoid re-processing of depended-upon modules, ensure they are loaded and so will be
           found should a process-cmt be invoked, by doing so in the original addition order
           ie. the last dep first at the bottom of recursion. *)
        let sorted = List.sort (fun (_k,gsig) (_k',gsig') ->
            compare
              gsig.gs_unique_id
              gsig'.gs_unique_id
          ) !all in

        (* run through the changed cmts in order of addition and update out-of-date info *)
        List.iter (fun (k, gsig) ->
            Printf.printf"CACHED %d %s  %s\n%!" gsig.gs_unique_id gsig.gs_modname gsig.gs_cmtfile) sorted;
        List.iter (fun (k, gsig) ->
            (* Printf.printf"REGEN? for %s %f/%f\n%!" gsig.gs_cmtfile (Unix.stat gsig.gs_cmtfile).st_mtime  gsig.gs_timestamp; *)
            if Sys.file_exists gsig.gs_cmtfile (* else go with it *)
               && (Unix.stat gsig.gs_cmtfile).st_mtime > gsig.gs_timestamp then(
              Printf.printf"REGEN for %s\n%!" gsig.gs_cmtfile;
              (* set the compilation environment as prevailed previously *)
              set_loadpath gsig.gs_loadpath (fun _->
                  set_flags gsig.gs_flags;
              (* regen the sig. don't need the result - cache update enough  *)
                  ignore@@ !process_cmt_fwd gsig.gs_modname gsig.gs_cmtfile);
              (* assert (not((Unix.stat gsig.gs_cmtfile).st_mtime > gsig.gs_timestamp)); *)
              ())
            else
let _=              Printf.printf"ADD CACHE for %s  %s\n%!" gsig.gs_modname gsig.gs_cmtfile in
              add_sig k gsig
          ) sorted;
*)
        (* if one contains structure then consider the whole cache as generated in debugger mode *)
(*
    if !all<>[] then begin
        let (_,gsig) = List.hd !all in
        if gsig.gs_structure<>None then ppx_mode:=false;
      end;
*)
        (* intended for insertion into an object but put aside for now *)
        sig_cache, pr_cache
     )                          (* try-finally exit *)
  with
  (* when reading cache:Sys_error or Failure("input_value: truncated object") *)
  | (* overwrite cache *) _ ->
(* Format.eprintf"CACHE RESTART  !!!!!!!!!!!@."; *)
     dirty_cache:=true;
     Hashtbl.create 5, Hashtbl.create 5
  (* | _-> Misc.fatal_error@@ "Genprint: corrupted "^ cachefile ^". Try deleting it." *)

let update_sig_cache()=
  let all = ref [] in
  (* filter cache of not-fully-processed sigs to allow for a re-attempt *)
  Hashtbl.iter (fun k gs-> if gs.gs_valid then all := (k,gs):: !all) sig_cache;
  Hashtbl.clear sig_cache;
  (* to avoid re-processing of depended-upon modules, ensure they are loaded and so will be
           found should a process-cmt be invoked, by doing so in the original addition order
           ie. the last dep first at the bottom of recursion. *)
  let sorted = List.sort (fun (_k,gsig) (_k',gsig') ->
                   compare
                     gsig.gs_unique_id
                     gsig'.gs_unique_id
                 ) !all in

  (* run through the changed cmts in order of addition and update out-of-date info *)
  List.iter (fun (k, gsig) ->
      (* Printf.printf"REGEN? for %s %f %f\n%!" gsig.gs_cmtfile (Unix.stat gsig.gs_cmtfile).st_mtime gsig.gs_timestamp ; *)
      if (Unix.stat gsig.gs_cmtfile).st_mtime > gsig.gs_timestamp then(
        Printf.printf"REGEN for %s\n%!" gsig.gs_cmtfile;
        (* set the compilation environment as prevailed previously *)
        set_loadpath gsig.gs_loadpath (fun _->
            set_flags gsig.gs_flags;
            (* regen the sig. don't need the result - cache update enough  *)
            ignore@@ !process_cmt_fwd gsig.gs_modname gsig.gs_cmtfile);
        (* assert (not((Unix.stat gsig.gs_cmtfile).st_mtime > gsig.gs_timestamp)); *)
        ())
      else
        Hashtbl.replace sig_cache k gsig
    ) sorted

let record_sig modname valid modsig file str =
  (* retain the unique id to preserve ordering *)
  let uid=try
    let gsig = Hashtbl.find sig_cache file in
(* Printf.printf"found uid %d for existing cache entry %s\n " gsig.gs_unique_id file; *)
    gsig.gs_unique_id
    with Not_found->
(* let _=Printf.printf"NO cache entry %s\n "  file in *)
      let uid= !cache_count in
      incr cache_count; uid
  in
  let gsig = {
      gs_modname=modname;
      gs_sig=modsig;
      gs_cmtfile=file;
      gs_timestamp=(Unix.stat file).st_mtime;
      gs_unique_id=uid;
      gs_loadpath=get_loadpath();
      gs_flags=get_flags();
      gs_structure=str;
      (* gs_structure= if !ppx_mode then None else str; *)
      gs_valid=valid;
    }
  in
(* Printf.printf"SAVE SIG  %s %f  %f\n" gsig.gs_cmtfile gsig.gs_timestamp ((Unix.stat file).st_mtime); *)
(* print_endline@@"adding to cache: "^file; *)
  Hashtbl.replace sig_cache file gsig;
  dirty_cache:=true;
  gsig

let save_cache()=
  if !dirty_cache then
    let validation = (Config.version, !stdlib_restricted) in
    let ch=open_out_bin cachefile in
    output_value ch validation;
(*
  let reduced_pr_cache = Hashtbl.(create (length pr_cache)) in
  Hashtbl.iter (fun k (r,p,ty,env) ->
      Hashtbl.replace reduced_pr_cache k (r,p,ty, Env.keep_only_summary env)) pr_cache;
  Hashtbl.filter_map_inplace (fun _k (r,p,t,e)->
                     Some (r,p,t,(*Env.keep_only_summary*) e) ) pr_cache;
*)

    Marshal.to_channel ch (sig_cache, pr_cache) [Closures];
    close_out ch

let find_mod m=
  let exception Return of string in
  let find k v=
    if v.gs_modname = m
    then raise (Return v.gs_cmtfile)
  in
  try
    Hashtbl.iter find sig_cache;
    raise Not_found
  with Return file->file


let find_cmt modname=
(* List.iter(fun p-> Printf.printf "LOADPATH [%s]\n" p) (get_loadpath()); *)
  (* this function tries both apped AND uncapped variants!  *)
  let file =
    try
      Misc.find_in_path_uncap (get_loadpath()) (modname ^ ".cmt")
    with Not_found->
      if robust then
        find_mod modname
      else
        raise Not_found
  in
  file


(* abandoned for now. needed assignment to a 'let rec' value disalllowed.
class ['a,'b,'c,'d] cache (fwd: string->string->Types.signature_item) =
  let (c,scache,pcache)=(process_cmt_fwd:=fwd;load_cache()) in
  object (self)
    constraint 'd = Path.t * Types.type_expr * Env.t
    val mutable cache_count= c
    val sig_cache : ('a,'b) Hashtbl.t = scache
    val pr_cache : ('c,'d) Hashtbl.t = pcache
    method find_sig k =Hashtbl.find sig_cache k
    method add_sig file modsig =
      let uid=try
          let gsig = self#find_sig file in
          gsig.gs_unique_id
        with Not_found->
          let uid= cache_count in cache_count <- 1+cache_count; uid
      in
      let gsig = {gs_sig=modsig;
                  gs_cmtfile=file;
                  gs_timestamp=(Unix.stat file).st_mtime;
                  gs_unique_id=uid;
                  gs_loadpath=get_loadpath();
                 }
      in
      Hashtbl.replace sig_cache file gsig

    method find_pr (k: int * string) =Hashtbl.find pr_cache k
    method add_pr k v = Hashtbl.replace pr_cache k v
  end
*)


(* store the cmi/crc's for this executable *)
#if OCAML_VERSION < (4,09,0)
let crc_interfaces = Consistbl.create ()
#else
module Consistbl = Consistbl.Make (Misc.Stdlib.String)
let crc_interfaces = Consistbl.create ()
#endif
(*
let interfaces = ref ([] : string list)

let add_import s =
  imported_units := StringSet.add s !imported_units

let store_infos cu =
  let store (name, crco) =
  let crc =
    match crco with
      None -> dummy_crc
    | Some crc -> add_import

  in
    printf "\t%s\t%s\n" crc name

  in
  List.iter store cu.cu_imports
*)

(* to accommodate use within a reloadable code environment.
   the consistency db is reset and the cmt tt info re-evaluated
   and possibly re-processed  when file stamp changes. *)
let refresh()=
  Consistbl.clear crc_interfaces;
  update_sig_cache()

let bytecode ic =
  Bytesections.read_toc ic;
  let toc = Bytesections.toc () in
  let toc = List.sort Stdlib.compare toc in
  List.iter
    (fun (section, _) ->
       try
         let len = Bytesections.seek_section ic section in
         if len > 0 then match section with
           | "CRCS" ->
             List.iter (function
                   | _, None->()
                   | name, Some (crc) ->
                     Consistbl.set crc_interfaces name crc ""
               )
               (input_value ic : (string * Digest.t option) list)

           | _->()
       with _ -> ()
    ) toc


(* populate the crc table *)
(* consistency checking by loading the infos of the running exec *)
let _=
  let prog = Sys.executable_name in
(*
  let prog =
    if Filename.is_relative prog then
      Filename.concat(Sys.getcwd()) (Filename.basename prog)
    else
      prog in
*)
  let ic = try
      open_in_bin prog
    with e->print_endline "error"; raise e
  in
  let len_magic_number = String.length Config.cmo_magic_number in

  (* assume a bytecode exec for now *)
  let pos_trailer = in_channel_length ic - len_magic_number in
  let _ = seek_in ic pos_trailer in
  let magic_number = really_input_string ic len_magic_number in

  if magic_number = Config.exec_magic_number then begin
      bytecode ic;
      close_in ic;
    end
  else
    (* a native exec does not carry the import info present in bytecode. fail or go on?!  *)
    (* failwith "Genprint: unknown excutable format" *)
    ()

(* match imports of cmt *)
let check_consistency cmt file=
  Cmt_format.(try
    List.iter
      (fun (name, crco) ->
         match crco with
            None -> ()
          | Some crc ->
              Consistbl.check crc_interfaces name crc "" (*cmt.cmt_sourcefile*))
      cmt.cmt_imports
#if OCAML_VERSION < (4,11,1)
  with Consistbl.Inconsistency(_name, _source, _auth) ->
#else
  with Consistbl.Inconsistency {
      unit_name = _name;
      inconsistent_source = _user;
      original_source = _auth;
    } ->
#endif
    failwith @@ "Genprint: inconsistency between "^ file ^" and this program")

(* intercept calls to particular functions in order to grab the types involved *)
#if OCAML_VERSION < (4,11,1)
let genprint = Longident.parse "Genprint.print"
let genprint_return = Longident.parse "Genprint.print_with_return"
let genprint_inline = Longident.parse "Genprint.print_inline"
let genprint_printer = Longident.parse "Genprint.install_printer"
let genprint_remove_printer = Longident.parse "Genprint.remove_printer"
#else
let genprint = Parse.longident (Lexing.from_string "Genprint.print")
let genprint_return = Parse.longident(Lexing.from_string "Genprint.print_with_return")
let genprint_inline = Parse.longident(Lexing.from_string "Genprint.print_inline")
let genprint_printer = Parse.longident(Lexing.from_string "Genprint.install_printer")
let genprint_remove_printer = Parse.longident(Lexing.from_string "Genprint.remove_printer")
#endif

(* while walking the structure note the embedded __context if any.
   composed of the loadpath and flags seen by the compiler.
 *)
(*
let embedded_info=ref None
(* if no embedding give back an empty loadpath plus existing flags *)
let no_info = 
  let flags_as_is = Array.map (fun r-> !r) flagrefs in
  [], flags_as_is

let extract_embedded_info _sub si =
  match si.str_desc with
  | Tstr_value(_,[{vb_pat={pat_desc=Tpat_var(id,_) };
                   vb_expr={
#if OCAML_VERSION < (4,11,1)
                       exp_desc=Texp_constant(Const_string(ser,_))
#else
                       exp_desc=Texp_constant(Const_string(ser,_,_))
#endif
                     }
    }]) when Ident.name id = "__context2" ->
     let inf = Marshal.from_string ser 0 in
     embedded_info:=Some inf;
#if OCAML_VERSION < (4,09,0)
     | _->()
#else
     | _ -> Tast_iterator.default_iterator.structure_item _sub si
#endif
*)

(* typedtrees are iterated over to find occurrences of [%pr] et al, associating type info with 
   them *)
let intercept_expression _sub exp=
    match exp with
    (* [%pr ... v] and [%prr ...] v  and [%prs ...] *)
    | {exp_desc = Texp_apply(
        {exp_desc = Texp_ident (p, lid, _)},
        [
          _;                     (* the string *)
          _, Some {exp_desc=Texp_tuple [ (* the value of any type, with extras stuffed in *)
              {exp_desc=Texp_constant(Const_int count)};
#if OCAML_VERSION < (4,11,1)
              {exp_desc=Texp_constant(Const_string(file,_))};
#else
              {exp_desc=Texp_constant(Const_string(file,_,_))};
#endif
              _;
           ]};
          (* though the ppx used two apply's it ends up merged  *)
          _,Some e
        ])
      ; exp_loc=_apploc}
      (* or ... *)
    | {exp_desc = Texp_apply(
       {exp_desc = Texp_apply(
        {exp_desc = Texp_ident (p, lid, _ )},
        [
          _;                     (* the string *)
          _, Some {exp_desc=Texp_tuple [ (* the value of any type, with extras stuffed in *)
              {exp_desc=Texp_constant(Const_int count)};
#if OCAML_VERSION < (4,11,1)
              {exp_desc=Texp_constant(Const_string(file,_))};
#else
              {exp_desc=Texp_constant(Const_string(file,_,_))};
#endif
              _;
           ]}
        ]);
        exp_loc=_apploc}, [_,Some e])}
         when lid.txt = genprint_return || lid.txt = genprint || lid.txt = genprint_inline ->

       let env =Envaux.env_of_only_summary e.exp_env in
(*
Printf.printf"count=%d, file=%s\n" count file;
Printtyp.type_expr Format.std_formatter e.exp_type;
print_endline "\n--------------";
Printf.printf"PR: %s/%d  (%d)\n" file count _apploc.loc_start.pos_cnum;flush stdout;
*)

(* if count=1 && file="jit.ml" then *)
(*
begin 
Format.eprintf">>>>>>>>>>>  PR type  %d(%s)@." count file;
Format.eprintf  "==========================================@.";
Printtyp.raw_type_expr Format.err_formatter e.exp_type;
Format.eprintf  "@.=== ... =======================================@.@.";
Printtyp.type_expr Format.err_formatter e.exp_type;
Format.eprintf  "@.===END =======================================@.@.";
end;
*)

       add_pr (count, file) (ref false, p, e.exp_type, env)

    | {exp_desc = Texp_apply(
        {exp_desc = Texp_ident (_,lid,_)},
        [
          _, Some ({exp_loc=loc} as fn);
          _, Some {exp_desc=Texp_tuple [ (* the value of any type, with extras stuffed in *)
              {exp_desc=Texp_constant(Const_int count)};
#if OCAML_VERSION < (4,11,1)
              {exp_desc=Texp_constant(Const_string(file,_))};
#else
              {exp_desc=Texp_constant(Const_string(file,_,_))};
#endif
              _;
           ]};
        ])}
      when lid.txt = genprint_printer || lid.txt = genprint_remove_printer ->

       (* due to the 'apply' context around the call not possible to prevent this case in ppx *)
       begin match fn with
       |{exp_desc=Texp_ident(fnpath,_,_); exp_type=ty; exp_env } ->
         (* hmmm, why now? should really expand once loadpth set-up which is actually
            bound as a stritem in each mod... so rather than pass loadpath to each
            print call, save in cache now. except(!), needed to find the cmt in the first place!
           *)
         let exp_env = Envaux.env_of_only_summary exp_env in
         (* let exp_env = exp_env in *)
(* Printf.printf"PR: %s/%d (%d)\n" file count loc.loc_start.pos_cnum;flush stdout; *)
         add_pr (count, file) (ref false, fnpath, ty, exp_env)
       | _-> 
#if OCAML_VERSION < (4,08,0)
          Location.(report_error
#else
          Location.(print_report
#endif
                      Format.err_formatter
                      (error ~loc "Genprint: must be a printer function name\n"));
                    failwith "aborting..."
             (* exit (-1) *)
       end
#if OCAML_VERSION < (4,09,0)
     | _->()
#else
     | other -> Tast_iterator.default_iterator.expr _sub other
#endif

(* by 4.08 typedtreeMap -> tast_mapper
   by 4.09 typedtreeMap -> tast_mapper, typedtreeIter -> tast_iterator
 *)
#if OCAML_VERSION < (4,09,0)
open TypedtreeIter
module M : IteratorArgument =
struct
  include DefaultIteratorArgument
  let enter_expression = intercept_expression ()
  (* let enter_structure_item = extract_embedded_info () *)
end
module I = MakeIterator(M)
#else
module I = struct
  let iter_structure = Tast_iterator.(default_iterator.structure
                         {default_iterator with
                           (* structure_item=extract_embedded_info; *)
                           expr=intercept_expression})
end
#endif

(*
let iter_structure str =
  embedded_info:=None;
  I.iter_structure str;
  match !embedded_info with
  | Some inf->inf
  | None-> no_info
*)
let iter_structure str =
  I.iter_structure str

let backtrace f a=
  Printexc.record_backtrace true;
  try
    f a
  with exn->
    print_endline "BACKTRACE.....";
    Printexc.print_backtrace stdout;
    raise exn



(* the order of visitation of modules will reflect for the most part the dependency
graph. when it comes to reprocessing updated cmts the order of that must respect 
the dependency order else stale typing info will be embedded in recomputed sigs.
 *)


(* abandoned attempt to ascertain whether an intf is actually abstracting any types -
assuming it does create a bit more work. but hey...
let cmi_abstraction env modname newsig=
  (* under dune the cmi will be another directory to the cmt for opt *)
  let file = Misc.find_in_path_uncap !search_dirs (modname ^ ".cmi") in
  try
    let cmi=Cmi_format.read_cmi file in
    let cmisig=cmi.cmi_sign in
    let check newitem=
      (* include sig-a sig-b ... sig-b is the specification that sig-a has to meet  *)
      (* try *)
      (* let unique_values it acc *)
        ignore@@ Includemod.(signatures env ~mark:Mark_neither cmisig [newitem] )
                   (* witn exn->raise exn *)
    in
    (* if modname="Stdlib_obj" then *)

    List.iter check newsig;
    print_endline@@"SIGMOD no change "^modname;
    false
  with exn->
    print_endline@@"SIGMOD has changed! "^modname;
    Location.report_exception Format.std_formatter exn;
    (* print_endline@@"->>>"^Printexc.to_string exn; *)
    true
*)

(* the cmt's of modules appearing in module-expressions are processed recursively
to anticipate use in functor applications which tends to yield new types,
while regular types are processed on demand by having the genprintval call out when
encountering an abstract type.

modules-for-tc is the recursive collection of modules depended upon, already processed
and for which there now exists an unabstracted signature. 
in this way the current module can be re-typechecked in the presence of those unabstracted
module signatures rather than abstracted cmi-located ones.
 *)
let modules_for_tc = ref []

(* shadowing is good enough *)
let subst orig sg =
  orig @ sg
(* this is wrong anyway as it needs to be names/category matched.
  let open Types in
  let sgids = List.map (function
        Sig_value(id, _)
      | Sig_type(id, _, _)
      | Sig_typext(id, _, _)
      | Sig_module(id, _, _)
      | Sig_modtype(id, _)
      | Sig_class(id, _, _)
      | Sig_class_type(id, _, _) as si -> (Ident.name id,si) ) sg
  in
if false then
  List.fold_right (fun si acc->
      match si with
        Sig_value(id, _)
      | Sig_type(id, _, _)
      | Sig_typext(id, _, _)
      | Sig_module(id, _, _)
      | Sig_modtype(id, _)
      | Sig_class(id, _, _)
      | Sig_class_type(id, _, _) ->
        let nusi = try List.assoc (Ident.name id) sgids with _-> si in
        nusi :: acc)
    orig []
*)

(* by 4.08 typedtreeMap -> tast_mapper
   by 4.09 typedtreeMap -> tast_mapper, typedtreeIter -> tast_iterator
 *)
#if OCAML_VERSION < (4,08,0)
module type S = module type of TypedtreeMap.MakeMap(TypedtreeMap.DefaultMapArgument)
open TypedtreeMap
#endif
(* module type S = module type of Ttmap.MakeMap(Ttmap.DefaultMapArgument) *)
(* identify earliest structure item changed due to removal of signature or a functor application
   composed of a global module(s) (assumed to be abstracting something relevant),
   and splitting the structure in two.
*)
let rec split (str:structure) =
  (* strip out constraints and process referenced modules' cmts *)
  let sz=List.length str.str_items in
  let count=ref 0 in
  (* note which items are altered *)
  let stritems_slots=Array.make sz false in

  (* the mapper will visit all levels but only want to recurse into idents when they are
     part of a functor application. other usages don't lead to new types. true?
  *)
  let proc_global env p=
    if Ident.global (Path.head p) then(
      module_for_tc env p;
      (* regard the module as being unabstracted and therefore material to
         the recompilation of current module *)
      stritems_slots.(!count) <- true);
  in

#if OCAML_VERSION < (4,08,0)
  let level=ref 0 in

  let unconstrain_mod_enter me=
    match me.mod_desc with
    | Tmod_apply _->
       incr level;
       me
    | _->me
  in
  let unconstrain_mod me=
    match me.mod_desc with
    | Tmod_ident(p,_lidloc) ->
(* Printf.printf "MIDENT %s %b/%b\n" (Path.name p)(Ident.global @@ Path.head p) (!level>0); *)
       (* only consider a mident when inside an application and then assume it was 
          abstracted in some way rather than troubling to track exactly how and if relevantly
          abstracted. *)
       if !level>0 then proc_global me.mod_env p;
       me

    | Tmod_apply(_fn,_farg,_c)->
       (* only ident modules referred to in functor applications *)
       decr level;
       me

    | Tmod_constraint(_me2,_mt,
                      (Tmodtype_explicit
                         {mty_desc=Tmty_with(_,cl)}), _mco) when
           List.exists(function (_,_, (Twith_typesubst _|Twith_modsubst _))->true
                              | _->false)
             cl
      -> me

    | Tmod_constraint(me2,_mt_ty, Tmodtype_explicit _, _mco)->
       (* dropping the abstracting sig *)
       stritems_slots.(!count) <- true;
       me2

(*
    | Tmod_constraint(me2,_mt_ty, Tmodtype_implicit, _mco)->
       print_endline "mod imp constraint"; me
    | Tmod_functor _->
       print_endline "mod functor"; me
    | Tmod_unpack _->
       print_endline "mod unpack"; me
    | Tmod_structure _->
       print_endline "mod struct"; me
*)
(*
    (* implicits added by tc? they can be left in place. doesn't count as a tree change as
       only to reach in to the idents *)
   | Tmod_constraint(me2,mt,Tmodtype_implicit,_)->
       (* ok to drop this auto-gen as untypeast strips it out anyway *)
*)
    | _->me
  in
  (* since the function override is specialised use a module-expr *)
  let (module Map : S) =
    (module MakeMap(
                struct
                  include DefaultMapArgument
                  (* when leaving else the tc will not see unabstracted components *)
                  let enter_module_expr = unconstrain_mod_enter
                  let leave_module_expr = unconstrain_mod
                end))
  in
  let remapped=List.map (function
                   | {str_desc=Tstr_recmodule _} as si-> incr count; si
                   | si ->
                      let si=Map.map_structure_item si in
                      incr count;
                      si)
                 str.str_items in

#else
  (* this mapper incorporates the overrides in the recursion so no double trouble *)
  let rec unconstrain_mod in_app sub me=
    match me.mod_desc with
    | Tmod_ident(p,_lidloc) ->
       (* Printf.eprintf "MIDENT %s %b/%b  alias? %b\n" (Path.name p)(Ident.global @@ Path.head p) (in_app) true; *)
       (* only functors lead to new types so unmask the constituents *)
       if in_app then proc_global me.mod_env p;
       me

    | Tmod_apply(fn,farg,c)->
       (* direct the mapper to only ident modules referred to in functor applications *)
       let sub={sub with Tast_mapper.module_expr=unconstrain_mod true} in
       let fn = unconstrain_mod true sub fn
       and farg = unconstrain_mod true sub farg in
       {me with mod_desc=Tmod_apply(fn,farg,c)}

    (* substitution constraints are essential to compilation and failure would
       leave the value abstract *)
    | Tmod_constraint(_me2,_mt,
                      (Tmodtype_explicit
                         {mty_desc=Tmty_with(_,cl)}), _mco) when
           List.exists(function (_,_, (Twith_typesubst _|Twith_modsubst _))->true
                              | _->false)
             cl
      -> me
    (* implicits added by tc? they can be left in place *)
    | Tmod_constraint(me2,_mt, (Tmodtype_explicit _), _mco)->
       (* dropping the abstracting sig and noting the change *)
       stritems_slots.(!count) <- true;
       (* must recurse this *)
       (* me2 *)
       unconstrain_mod in_app sub me2

    | _ -> Tast_mapper.default.module_expr sub me
  in
  let mapper=Tast_mapper.{default with module_expr=unconstrain_mod false} in
  let remapped=List.map (function
                   (* recursive mods always need a type? *)
                   (* I don't remember why I singled this out!
                      in case possible rec bindings can be written without constraint
                      why not just process it anyway... *)
                   | {str_desc=Tstr_recmodule _} as si->incr count; si
                   | si ->
(*
begin match si.str_desc with
Tstr_module {mb_id}->
  Option.iter(fun id->
if true||Ident.name id = "Core_kernel__Set" then Printf.printf "STR MOD: %s %d\n%!" (Ident.name id) !count) mb_id
| _->()
end;
*)
                      let si=mapper.structure_item mapper si in
                      incr count;
                      si)
                 str.str_items
  in
#endif
  (* nb. the str_env is the env resulting _after_ typechecking of the item. *)
  (* find the earliest stritem modified *)
  let mem x a =
    let open Array in
    let n = length a in
    let rec loop i =
      if i = n then raise Not_found
      else if compare (get a i) x = 0 then i
      (* else if compare (unsafe_get a i) x = 0 then i *)
      else loop (succ i) in
    loop 0
  in
  try
    (* must tc all from 1st changed item so scan results array *)
    let i = mem true stritems_slots in
(* let i=0 in *)
    let unchanged,changed = let n=ref 0 in List.partition (fun _-> incr n; !n<=i) remapped in
    (unchanged,changed)
  with Not_found->
    (* otherwise no mods - no items to tc *)
    (remapped, [])

(*
and count_sig_items stris =
  let add stri =
    match stri.str_desc with
    | Tstr_eval _-> 0
    | Tstr_include i->
       List.length i.incl_type
    | _-> 1
  in
  List.fold_left (+) 0 stris
*)


and process_cmt modname file=
  try
    process_cmt_aux modname file
  with e->
    prerr_endline "unexpected exception - BUG!";
    prerr_endline@@ " ==> "^ Printexc.to_string e;
    exit (-1)

and process_cmt_aux modname file=
  let valid = ref true in
  (* if from stdlib (bar exceptions) don't need to disassemble.
     by default other installed libs ARE disassembled unless 
     GENPRINT_INSTALLED_LIBS_OPAQUE is set.
     so 'restricted' in sense of prevented from being disassembled.
  *)
  let restricted = is_restricted file in
(* Format.printf "PROC CMT: %s (%s) restricted? %b@." modname file restricted; *)
  let cmt = Cmt_format.read_cmt file in
  check_consistency cmt file;
  let str, _loadpaths = match cmt with
    | {cmt_annots=Implementation str; cmt_loadpath=lp}-> str,lp
    | _ ->
       (* if it didn't compile how can there be an exec? *)
       failwith ("Genprint: "^modname^".cmt file is not complete. Failed compilation?")
  in
  (* re-establish loadpaths as when originally compiled so avoiding namespace clash,
    particularly in respect to shadowing stdlib mods! *)
  (* set_loadpath loadpaths @@ fun ()-> *)

  let valid_deps, str, sign, _changed =
      (* when the module is of the stdlib/libdir need only obtain the struct sig *)
    if restricted then begin
      iter_structure str;
      (* assume a library module does not need any processing other than by dodging its
         .mli derived .cmi *)
      true, str, str.str_type, false
    end else
      (* save the state for calling function *)
      let pre_sigs = !modules_for_tc in
      modules_for_tc:=[];
      (* split the structure according presence of functor applications/sig removal *)
      let unchanged, changed = split str in
      (* Format.eprintf "SPLIT         %s  %d(%d/%d) usig=%d@." modname (List.length str.str_items)(List.length unchanged)(List.length changed)(List.length unchanged_sig); *)

      let changed_str, sign =
        (* recompile only those changed structure items  *)
        let str={str with str_items=changed}in
        let when_no_tc = (str, []) in

(* relating to labels:
Clflags.classic:=true;
Printf.eprintf "TC for %s %b\n" modname (changed=[]);
*)
        if changed<>[] then        (* something to tc *)
          try
            (* env-of often fails for lack of a path to a cmi *)
            let tc_env = Envaux.env_of_only_summary (List.hd changed).str_env in
            let pstr = Untypeast.untype_structure str in
            let sigdeps = List.map (fun gs -> gs.gs_sig) (!modules_for_tc) in
            (* bring into env all modules faulted in via functor application
               so that new types are non-abstract *)
            let uniq_mods = List.fold_left (fun acc sg ->
                                if List.memq sg acc then acc else sg::acc)
                              [] sigdeps in
            let senv = Env.add_signature (List.rev uniq_mods) tc_env in
#if OCAML_VERSION < (4,08,0)
             let changed_str, sign, _env = Typemod.type_structure senv pstr Location.none in
#elif OCAML_VERSION < (4,12,0)
             let changed_str, sign, _, _env = Typemod.type_structure senv pstr Location.none in
#elif OCAML_VERSION < (4,14,0)
             let changed_str, sign, _, _env = Typemod.type_structure senv pstr in
#elif OCAML_VERSION < (4,15,0)
             let changed_str, sign, _, _, _env = Typemod.type_structure senv pstr in
#else
             let changed_str, sign, _, _, _env = Typemod.type_structure senv pstr in
#endif
             changed_str, sign
          with
          | Not_found ->
             if not !ignore_missing then
               prerr_endline ("Genprint: unable to process module "^modname
                              ^" - the cmt/load-path probably not correct.\n");
             (* previously the fallthrough of Notfound would <abstr> the originating
                abstract type even if current module may have nothing to do with it.
                this is because whole file is being processed.
                the thing to do is not record the sig. *)
             valid:=false;
             when_no_tc
          (* the rest are regarded as 'valid' as nothing can be done about
             by user so might as well cache it and never try to re-process *)
          | Typemod.(Error(_,_,Repeated_name _)) ->
             (* alas the removal of constraining signatures presents the
                issue of revealing values that happen to be shadowed in
                the scope into which they are injected.
                the avoidance of trouble with 'open!' is not available
                in other forms where shadowing is risked.
                only possible workaround imagined is the rewriting of the
                constraint from the types ie. typedtree from types.
                but that looks like a minefield so it is accepted here
                that some modules are just not going to be unabstracted.*)
            when_no_tc
          | Typemod.(Error(_,_,Not_included _))->
             (* Base.Int63 @immediate  *)
            when_no_tc
(* resolved- just leave them as is.
          | Typemod.(Error(_,_,Recursive_module_require_explicit_type))->
             (* Async_kernel.Types *)
            when_no_tc
*)
          | Typecore.(Error(_,_,Expr_type_clash _)) ->
             (* Info.t escaping type Message.t lazy_t *)
             (* ...but the type is exposed in intf anyway *)
            when_no_tc
          | exn ->
             prerr_endline ("Genprint: unable to process module "^modname
                                                                    (* ^"\n"); *)
                            ^" - please file an issue!\n");
             if true (*debug*) then
              (Printexc.print
                (Location.report_exception Format.err_formatter) exn;
               if debug then assert false);
             when_no_tc
        else
          (* no need for any tc *)
          when_no_tc
      in
(*
Printf.printf "STATS: %s ==> unch=%d chg=%d ==%d, partstr=%d sig=%d origsig=%d  mods-for-tc=%d\n" 
  modname
      (List.length unchanged)
      (List.length changed)
      (List.length str.str_items)
      (List.length changed_str.str_items)
      (List.length sign)
      (List.length str.str_type)
      (List.length !modules_for_tc)
;
*)


(*
if true then begin
Format.eprintf  "== RECOMPED %s %d ========================================@." modname (List.length !modules_for_tc);
(* end; *)
Printtyp.signature Format.err_formatter sign;
Format.eprintf  "@.===END %s =======================================@.@." modname;
end;
*)

      let valid_deps = List.for_all (fun gs-> gs.gs_valid) !modules_for_tc in
      (* restore caller state *)
      modules_for_tc:=pre_sigs;
      (* recompose the two halves of the structure, unchanged and the re-tc'd *)
      let newstr={changed_str with str_items=unchanged@ changed_str.str_items} in
      (* collection of %pr's now with unabstracted types *)
      iter_structure newstr;
      (* subst - actually just shadow the old decls *)
      valid_deps, newstr, (subst str.str_type sign), changed<>[]
    in
    (* unalias and add a new signature *)
    let sign = subst_aliases str.str_final_env sign in
    let modid = Ident.create_persistent modname in
    let md_loc = Location.none in
    let modsig=
#if OCAML_VERSION < (4,08,0)
      Types.Sig_module(modid,
                       {md_type = Mty_signature sign;
                        md_attributes = [];
                        md_loc;
                       },
                       Trec_not
      )
#else
      Types.Sig_module(modid, Mp_present,
                       {md_type = Mty_signature sign;
                        md_attributes = [];
                        md_loc;
#if OCAML_VERSION >= (4,11,1)
                        md_uid=Types.Uid.internal_not_actually_unique;
#endif
                       },
                       Trec_not,
                       Exported
                      (* Hidden? *)
      )
#endif
    in
    let valid = !valid && valid_deps in
    record_sig modname valid modsig file str

(* replace aliases to persistent mods with their unabstracted signatures *)
and subst_aliases env sign=
  let open Types in
  let subst =function
#if OCAML_VERSION < (4,08,0)
    | Sig_module(id,
                 ({md_type=Types.Mty_alias(_,p)} as _x),
                 _rec) as si ->
       let p=normalize_path env p in
       (* Format.eprintf"SUBST %s %b@." (Path.name p)(Ident.global(Path.head p)); *)
       if Ident.global(Path.head p) then
         let sg1=find_sig (Ident.name(Path.head p)) in
         let env=Env.add_signature [sg1] env in
         let md=Env.find_module p env in
         (* change to 'present' avoids raise from Env.module_declaration_address *)
         Sig_module(id,{_x with md_type=md.md_type},_rec)
(* sg1 (\*this works too *\) *)
       else si
    (* allow for inner occurrences *)
    | Sig_module(id,({md_type=Types.Mty_signature sg} as md),_rec) ->
       Sig_module(id,
                  {md with md_type=Types.Mty_signature (subst_aliases env sg)},_rec)
#else
    | Sig_module(id,pres,
                 ({md_type=Types.Mty_alias p} as _x),
                 _rec,vis) as si ->
       let p=normalize_path env p in
       (* Format.eprintf"SUBST %s %b@." (Path.name p)(Ident.global(Path.head p)); *)
       if Ident.global(Path.head p) then
         try
           let sg1=
             (* dune strikes again - missing .cmt file blows it up *)
             find_sig (Ident.name(Path.head p))
           in
           let env=Env.add_signature [sg1] env in
           let md=Env.find_module p env in
           (* change to 'present' avoids raise from Env.module_declaration_address *)
           Sig_module(id,Mp_present,{_x with md_type=md.md_type},_rec,vis)
         with Not_found-> si    (* the cmt ought to have been there but since not, return
                                   what's available thus far *)
       else si
    (* allow for inner occurrences *)
    | Sig_module(id,pres,({md_type=Types.Mty_signature sg} as md),rec_,vis) ->
       Sig_module(id,pres,
                  {md with md_type=Types.Mty_signature (subst_aliases env sg)},rec_,vis)
#endif
    | si->si
  in
  List.map subst sign

(* the module in which a %pr appears doesn't need to augment an env with its sig
   as each %pr will pick up a new env directly from regenerated typedtree. *)

(* a visited file's generated signature *)
and find_gsig modname cmtfile=
  (* module names are resolved in the context of the current loadpath so
     in case of name re-use better to use the path as a key. *)
  try
    find_globalsig cmtfile
  with Not_found->
    process_cmt modname cmtfile

and find_gsig2 modname=
  let cmtfile = find_cmt modname in
  find_gsig modname cmtfile

and find_sig modname=
  let cmtfile = find_cmt modname in
  let gsig = find_gsig modname cmtfile in
  gsig.gs_sig

(* no longer needed as last chance saloon on dune wrapping put directly into print/install.
and process_local_cmt modname=
    (* cannot be called if a script/interactive, as structure scanning already done *)
(* print_endline@@"PROC LOCAL CMT: "^modname^"    "^ !Location.input_name; *)
  ignore@@
    try
      find_sig modname
    with Not_found->
      failwith("Genprint: No .cmt file found corresponding to "^modname)
*)

and module_for_tc env p =
  let p=normalize_path env p in
  let modid=Path.head p in
  let modname=Ident.name modid in
  try
    let sg = find_gsig2 modname in
    (* add sig to the accummulating env for recompiling target module *)
    modules_for_tc := sg :: !modules_for_tc;
  with Not_found->
    (* if non-existent just allow to remain abstract which will be
       intercepted in [unabstract_type] and an <abstr> returned *)
    ()


(* fwd decl probably unnecessary but too much in the way for now *)
let _= process_cmt_fwd:=process_cmt

(* slightly more convenient than dune clean et al,
allows cache to be rebuilt from current disk cmt's
 *)
let _=
  if envar_set "GENPRINT_NO_CACHE"
  then print_endline "Genprint: cache not loaded"
  else ignore@@ load_cache()

(* lifted out in order to deal with functor derived types - F(A).t   *)
(*
let rec unabstract_by_path_component env = function
  | Path.Pident modid->
     if not@@Ident.global modid then (* not going to find a cmt for it *) raise Not_found;
     let modname = Ident.name modid in 
     let modsig = find_sig modname in
     (* references to this module will now avoid consulting the .cmi *)
     Env.add_signature [modsig] env
#if OCAML_VERSION < (4,08,0)
    | Pdot(p,_,_) ->
#else
    | Pdot(p,_) ->
#endif
     unabstract_by_path_component env p
  | Papply(f,fa) as p->
Format.eprintf"P-APPLY %s@." (Path.name p);
(*
let _=raise Not_found in
*)
     (* it is ok for either of f/fa to be non-global -
        as long as one, the other, or both, yields unabstraction.
        no-change is caught later. *)
     let newenv = 
       try unabstract_by_path_component env fa with Not_found -> env in
     try unabstract_by_path_component newenv f with Not_found-> newenv
 (* env *)
*)

(* side-step the abstract types of a cmi to get at the declarations *)
let unabstract_type p env mkout =

(* Format.eprintf "UNABSTRACTION FAULT: %s %b@." (Path.name p) (try ignore@@Path.head p;true with _-> false); *)
(*
Format.eprintf "load path...";
List.iter (fun i-> Format.eprintf "LOAD: %s@." i) (get_loadpath());
*)

  (* special case needed for JS Core-kernel - this mod uses Obj.magic under the hood
     and cannot be uncovered! *)
  if Path.name p="Thread_safe_queue.t" 
  then Outcometree.Oval_stuff"< ? >" else begin


  let modid = try Path.head p with _-> raise Not_found in (* F(M).t irredeemably abstract *)
  if not@@Ident.global modid then      (*  *) raise Not_found;
  let modname = Ident.name modid in 
  let modsig = find_sig modname in
  (* references to this module will now avoid consulting the .cmi *)
  let newenv =   Env.add_signature [modsig] env in

  (* is the wanted type still abstract? Bigarray.Genarray.t is example of external/opaque *)
  begin
    let decl = Env.find_type p newenv in
    match decl with
    | {type_kind = Type_abstract; type_manifest = None} ->
(* Format.eprintf"STILL ABSTRACT !!!!!!!!!!!!!!! %s@." (Path.name p); *)
       raise Not_found (* <abstr> *)
    | _-> ()
  end;
  (* remove the exact type leaving the module path *)
  let p = match p with
#if OCAML_VERSION < (4,08,0)
    | Pdot(p,_,_) -> p
#else
    | Pdot(p,_) -> p
#endif
    | _ -> assert false in

  (* open the just added module to avoid repetitive prefixing  *)
  let newenv =
    (* without_cmis shouldn't be needed as the modid is defined now *)
#if OCAML_VERSION < (4,06,0)
    let mdecl=Env.find_module p newenv in
    match mdecl.md_type with
    | Mty_signature sg ->
       Env.(without_cmis (open_signature Fresh p) sg newenv)
    | _ -> assert false
#else
    match Env.(without_cmis (open_signature Fresh p) newenv) with
#if OCAML_VERSION < (4,11,1)
    | Some env->env
#else
    | Ok env->env
#endif
    | _->assert false 
#endif
    | exception Not_found -> assert false
  in

  let open Outcometree in
  let printer ppf = 
    (* type name not wanted, only the path preceding it *)
    let modname =
      (* Oprint puts out Stdlib__xxxx so this is inconsistent with that ...*)
#if OCAML_VERSION >= (4,07,0)
      Printtyp.rewrite_double_underscore_paths newenv p |> Path.name
#else
      Path.name p
#endif
    in
    let wrap out=
      Format.fprintf ppf "%s." modname;
      !Oprint.out_value ppf out
    in
    (* want M.t value to display as M.(v) when no curlies/parenths/brackets *)
      (* rerun the printing with the augmented env *)
    if envar_set "GENPRINT_QUALIFY" then 
      match mkout newenv with
      | Oval_stuff "<abstr>" as abs -> !Oprint.out_value ppf abs (* no wrapping of this *)
      (* | Oval_constr _ *)
      | Oval_record _
        | Oval_variant _
      (* as it was overcoming abstraction that brought us here, prepend the module path for these
         too *)
      | Oval_stuff _ | Oval_tuple _ | Oval_array _ as l -> wrap l
      (* == use parentheses *)
      | out -> wrap @@ Oval_tuple [out]
    else
      !Oprint.out_value ppf (mkout newenv)
  in
  Oval_printer printer
 end

module EvalPath = struct
  type valu = Obj.t
  exception Error
(*
  let eval_path env p = try eval_path env p with Symtable.Error _ -> raise Error
  let same_value v1 v2 = (v1 == v2)
*)
 let eval_address _addr = Obj.repr 0
 let eval_path _env _p = Obj.repr 0
 (* let same_value v1 v2 = (v1 == v2) *)
 (* as this is originally for the toplevel not sure if it is relevant here.
    assuming homonyms not possible and extension paths always resolve uniquely *)
 let same_value _v1 _v2 = true

 let unabstract_type = unabstract_type
end

module LocalPrinter = Genprintval.Make(Obj)(EvalPath)

(* as per the defaults of the toploop *)
let max_printer_depth = ref 100
let max_printer_steps = ref 300
let formatter= ref Format.std_formatter


(* genprintval from the ocaml src is copied verbatim as not possible to have
   toplevel lib in opt form without hassle. *)
let outval_of_value env obj ty =
  LocalPrinter.outval_of_value !max_printer_steps !max_printer_depth
    (fun _ _ _ -> None) env obj ty

let print_value env obj ppf ty =
  !Oprint.out_value ppf (outval_of_value env obj ty)

let printing_enabled= not@@ envar_set "GENPRINT_NOPRINT"


(* well, setting loadpath can interfere with an application also making use of compiler-libs!
   loadpath now protected for duration of f
 *)
let unpack ?(debugger=false) ctxt f =
  (* NOT protecting the flags though *)
  set_flags (ctxt.compile_flags);
  (* the debugger already records search paths *)
  if not debugger then set_loadpath ctxt.loadpath f


let mkmodname file =
  Filename.remove_extension file
  |> String.capitalize_ascii

(* shared by printing/printer installation.
print/install is expected to fail the first time either happens in a file
- the .cmt is then read in and scanned for occurrences of either.
then the same fn is tried again and if it fails again or if
a suitably named .cmt cannot be found and raises, it maybe because of Dune
wrapping. that cannot be detected directly but if compilation is through Dune
then another attempt is made.
the module-file name is adjusted by any use of the -open flag, as is done in 
wrapping, and another attempt is made. 
it's a hack until/if compiler one day passes along its -o option
that allows to connect source file with build artefact names.
 *)
let find_and_process fn ctxt srcfile =
  let modname= mkmodname srcfile in
  let tryfind name =
    if debug then Printf.printf "Genprint: Searching for %s as %s\n%!" modname name;
    ignore@@find_gsig2 name;
    fn();
  in
  let rec until_first f =function
      | []->raise Not_found
      | h::t-> try f h with Not_found-> until_first f t
  in
  if debug then(
    print_endline "Genprint LOADPATH...";
#if OCAML_VERSION < (4,08,0)
    List.iter (fun i-> Printf.printf " DIR: %s\n" i) (!Config.load_path);
#else
    List.iter (fun i-> Printf.printf " DIR: %s\n" i) (Load_path.get_paths());
#endif
  );

  (* process should combine consistency check and collecting of %pr's.
     for abtracted-type-faulting only the sig is wanted*)
  try

    (* if ctxt.dune then *)
      (* this covers exec and lib wrapping in dune *)
      until_first tryfind
        (* open-modules will be [] if dune wrapping disabled. *)
(* hmmmm ... Debugger open module is 'Debugger' while ecaml/app is 'App__', tests/bug is Dyn__ *)
(* 
https://github.com/ocaml/dune/issues/5007
-open X__ when user supplies frontend aliasing module
-open X   when not provided but generated by dune
 *)
(*
jit__.cmi looks to be auto-generated by dune
jit.cmi is the real one
 *)
        (List.fold_left (fun acc s-> 
             let l=String.length s in
             let prefix=
               if l>1 && s.[l-1]='_' && s.[l-2]='_' then s
               else s^"__" in
             (prefix^modname)::acc) [modname] ("dune__exe__":: ctxt.open_modules))
  with Not_found->
    Printf.eprintf "Genprint: cannot find .cmt file for %s\n" modname;
    exit(-2)

(*
print_endline "load path...";
List.iter (fun i-> Printf.printf "LOAD: %s\n" i) loadpath;
print_endline "--- cwd...";
  print_endline @@"run directory: "^Sys.getcwd();
*)

let use_prompt=ref "=>"
let prompt s= use_prompt:=s

let flush_at_once=ref true

(* let unsummarize env = try Envaux.env_of_only_summary env with _-> env *)

(*
put out a string identifier, then the value on next line.
ppx knows the src being processed, runs a count to distinguish applications of pr.
as the target value is 'a, the count/file can piggyback it while keeping the types straight.
*)
let print_joint inline s (count,srcfile,ctxt) (v: 'a) : unit =
  let ppf = ! formatter in
  let print()=
    let key = (count,srcfile) in
    let ready, _p,ty,env = find_pr key in
(*
print_endline@@"GP pre-find for "^srcfile;
Printf.printf"GP %d src=%s %s \n" count ctxt.source srcfile ;
List.iter (Printf.printf"  loadpath: %s\n") ctxt.loadpath;
print_endline@@"GP post-find for "^srcfile;
if count=1 && srcfile="jit.ml" then begin
Format.fprintf ppf "--TYPE for %d(%s) %d ------ " count srcfile (Hashtbl.length pr_cache);
Printtyp.type_expr ppf ty;
Format.fprintf ppf "@.";
end;
*)
    (* the print format is limited and ugly - ideal for dissuading users from actually using this
       for anything other than debugging. *)
    if inline then
      Format.fprintf ppf "%s" s
    else
      Format.fprintf ppf "%s%s " s !use_prompt;
    (* dependency on toploop removed because opt version not available. *)
    (* Toploop.print_value env v ppf ty; *)
    (* a minor efficiency gain - once a print stmt has been run once all
       necessary cmt's are in the cache processed, so no need for loadpath
       set-up again. with version >4.07, Load_path takes a bit of processing to set-up
       then restore. *)
    if !ready then print_value env (Obj.repr v) ppf ty
    else begin
        (* let env = unsummarize env in *)
        unpack ctxt (fun _-> print_value env (Obj.repr v) ppf ty);
        (* ...except it doesn't work - the Env machinery probably still needs to
           see the .cmis at least once.
           !rectified by resetting false when loading cache.
         *)
        (* add_pr key (ref true, _p,ty,env); *)
        ready:=true; 
      end;
    if not inline then 
      if !flush_at_once then
        Format.fprintf ppf "@."
      else
        Format.fprintf ppf "@\n"
    else
    if !flush_at_once then
      Format.fprintf ppf "@?"
  in
  try
    (* the first print of the module executed will fault *)
    print()
  with Not_found->
    (* doesn't work without the loadpath setup! so the cache envs are unique to the file's
       loadpaths and cannot be meddled with ie. summary-of, prior *)
    (* init_cache(); *)
    (* loadpath stored as value in each module then transmitted through each print tuple to here
       but only needed once per module *)
    (* set_loadpath loadpath; *)
    unpack ctxt (fun _-> find_and_process print ctxt srcfile)

let print             s i v = if printing_enabled then print_joint false s i v
let print_with_return s i v = if printing_enabled then print_joint false s i v; v
let print_inline s i v = if printing_enabled then print_joint true s i v
let print_inline_end s =
 if not printing_enabled then () else
   let ppf = !formatter in
   if !flush_at_once then
     Format.fprintf ppf "%s@." s
   else
     Format.fprintf ppf "%s@\n" s

type 'a printer_type = Format.formatter -> 'a -> unit

let printer_type env =
#if OCAML_VERSION < (4,10,0)
  Env.lookup_type (Ldot(Lident "Genprint", "printer_type")) env
#else
  fst @@ Env.lookup_type ~loc:Location.none (Ldot(Lident "Genprint", "printer_type")) env
#endif

let match_simple_printer_type env ty printer_type =
  Ctype.begin_def();
  let ty_arg = Ctype.newvar() in
  Ctype.unify env
    (Ctype.newconstr printer_type [ty_arg])
#if OCAML_VERSION < (4,08,0)
    (Ctype.instance_def ty);
#else
    (Ctype.instance ty);
#endif
  Ctype.end_def();
  Ctype.generalize ty_arg;
  (ty_arg, None)


let match_printer_type env p =
  let vd= Env.find_value p env in
  let printer_type_new = printer_type env in
#if OCAML_VERSION < (4,08,0)
  Ctype.init_def(Ident.current_time());
#endif
  match_simple_printer_type env vd.val_type printer_type_new


let printer_joint install fn (count,srcfile,ctxt) =
  (* need an Obj.t of value to be printed *)
  (* let fn : 'a printer_type = magic fn in *)
  let fn ppf v = fn ppf (Obj.obj v) in
  let install()=
    let key = (count,srcfile) in
    let ready, p,_ty,env = find_pr key in (* ty not needed *)
    let (ty_arg, ty) = match_printer_type env p in
    match ty with
    | None ->
       if install then
         LocalPrinter.install_printer p ty_arg fn
       else
         LocalPrinter.remove_printer p
    | _-> assert false
  in
(*  
  let modname =
    Filename.remove_extension srcfile
    |> String.capitalize_ascii in
  process_local_cmt modname;
  install()
in
*)
  try
    unpack ctxt install
  with Not_found | Env.Error _->
    unpack ctxt (fun _->find_and_process install ctxt srcfile)

let install_printer fn inf = if printing_enabled then printer_joint true fn inf
let remove_printer fn inf = if printing_enabled then printer_joint false fn inf

let _=
  at_exit save_cache


(* for debugger.
   the idea is use an event location in place of a %pr to identify scope and thus extract
   an appropriate env.
 *)
let refloc= ref Location.none

exception Found of Env.t
open Location
let compare {loc_start=rstart;loc_end=rend} {loc_start=xstart;loc_end=xend} =
  rstart.pos_cnum = xstart.pos_cnum
  && rend.pos_cnum = xend.pos_cnum

#if OCAML_VERSION < (4,09,0)
open TypedtreeIter
module M2 : IteratorArgument =
struct
  include DefaultIteratorArgument
  let scan_for_location exp=
    match exp with
(*
    | {exp_desc=Texp_ident(p,lidloc,vd); exp_loc=loc;exp_env=env} ->
      if lidloc.txt = Lident "id" then(
        Format.(printf"\nTESTING....%s\n" (Path.name p) ;
                (* Location.print_loc std_formatter loc; *)
                printf "================@.\n";flush stdout;
print_newline();
       let env =Envaux.env_of_only_summary env in
                let _ = Env.lookup_value (Lident "id") env in
                (* let _ = Env.find_value p env in *)
                printf "id match \n";
                ()))
*)
    | {exp_loc=loc;exp_env=env} ->
      if compare !refloc loc then
        raise (Found(Envaux.env_of_only_summary env))

  let enter_expression = scan_for_location
end
module I2 = MakeIterator(M2)

#else
module I2 = struct
  let scan_for_location sub exp=
    match exp with
    | {exp_loc=loc;exp_env=env} ->
      if compare !refloc loc then
        raise (Found(Envaux.env_of_only_summary env));
      Tast_iterator.default_iterator.expr sub exp

  let iter_structure = Tast_iterator.(default_iterator.structure
                         {default_iterator with
                           expr=scan_for_location})
end
#endif

(* debugger interface.*)

let unpack ctxt f =
  match ctxt with
  |Some ctxt-> unpack ~debugger:true ctxt f;
(*
    List.iter(fun d->Printf.printf" INFO ACTUAL: %s\n" d) !Config.load_path;
    List.iter(fun d->Printf.printf" INFO: %s\n" d) loadpath
*)
  |None->()

(* when an id encountered in debugger for which the type is recorded abstract,
   it is enough to find and process the cmt for that module, and shadow
   the prior declaration in the environment. if the id is used with an accessor,
   so is a record, string, or array, then only one attempt at unabstraction can
   be tolerated, as with printing, else a loop is entered.
each level of eval.expression requires this one-chance approach.
the existing func can be renamed with a new 1st level func doing the abstraction/
unabstraction test/processing. unfortunately the expr type only comes from
recursing on it... so the 1st level guard cannot act in isolation.
ty is going to be a tconstr of some description.
the predefines are ok, any other is checked for abstraction.
but checking the latter is enough fr all.

*)
let debugger_unabstract_global path (ctxt: context option) env =
  (* ppx mode check removed as first stop will do it. *)
  let hd= Path.head path in
(*
#if OCAML_VERSION < (4,08,0)
  if Ident.global hd then
#else
  if Ident.global hd && not(Ident.is_predef hd) then
#endif
*)
  if Ident.persistent hd then
    let modname = Ident.name hd in
    let nu = ref Env.empty in
    unpack ctxt (fun _->
    (* ensure the cmt corresponding to the debugger frame is processed along with dependencies *)
        let msig= find_sig modname in
(*
Format.(
  printf "@[---SIGGY--%s-@\n" modname;
  Printtyp.signature std_formatter [msig];
  printf "@\n---@]@\n";
);
*)
        nu:= Env.add_signature [msig] env);
    !nu
  else env

(*
let rec path_to_sig env td p =
  let open Path in
  let open Types in
  match p with 
    Pident id-> 
    let p = try Env.lookup_type (Lident(Ident.name id)) env with _-> assert false in
let id=Path.head p in
Sig_type(id,td,Trec_not)
  | Pdot(p,s,_) ->
    let mp = try Env.lookup_module ~load:false (Lident s) env with _->assert false in
Printf.printf"P2SIG: %s\n" s;
    let mpid = Path.head mp in
    let sgi = path_to_sig env td p in
    let mt = Mty_signature [sgi] in
    let md = {md_type=mt; md_attributes=[]; md_loc=Location.none} in
    Sig_module(mpid, md, Trec_not)
  | _->assert false
*)

(* this interface provides for stopping at a breakpoint and therefore within some
   module. there may be values defined locally that require unabstraction but they
   have ids assigned which can appear in the event environment so to remain consistent 
   with it the new definitions are looked up for their existing ids with which to tag
   them and shadow the old in the returned environment. *)
let debugger_unabstract_local loc modname (ctxt: context option) _env =
(*
  if !ppx_mode then begin
      empty_cache();
      ppx_mode:=false;
      print_endline "resetting Genprint cache";
    end;
*)
  let nu=ref Env.empty in
  unpack ctxt (fun _->
      let gsig= find_gsig2 modname in
      refloc:=loc;                  (* setup for search of this loc *)
      try
        I2.iter_structure gsig.gs_structure;
        assert false
(*
    match gsig.gs_structure with
    | Some str -> 
    | None -> assert false
 *)
      with Found env -> nu:=env);
  !nu


(*
   the loc fname could be used to differentiate between identically named modules living
   in the same project. but with cwd prepended it's dirname is not on the _build path
   and not therefore right for limiting the search space for a corresponding cmt file 
   (not stored alongside src under dune).
   so using only the module name for now.
 *)

(* how to arrange for particular exceptions from compiler infrastructure:
  | Cmi_format.Error e ->
      eprintf "Debugger [version %s] environment error:@ @[@;" Config.version;
      Cmi_format.report_error err_formatter e;
      eprintf "@]@.";
      exit 2

or centrally:
  with x ->
    Location.report_exception ppf x;
    exit 2
*)

(* let _=
 *   Printexc.record_backtrace true *)

let scan_phrase str=
  iter_structure str

let filter_phrase =
  List.filter (function
      | { str_desc =
            Tstr_value
              (Asttypes.Nonrecursive,
               [{vb_pat = {pat_desc=Tpat_var(id,_)}}
               ]
              )
        }
           (* chamge to preix of __context... *)
        when Ident.name id="__context" ->false
      | _->true)

