open Genprintaux

let cmtpath=cmtpath
let all_libs_opaque= all_libs_opaque
let max_printer_depth=max_printer_depth
let max_printer_steps=max_printer_steps
let formatter=formatter
let flush_at_once = flush_at_once
let prompt=prompt

type t = Genprintaux.t

let print=print
let print_inline=print_inline
let print_inline_end=print_inline_end
let print_with_return=print_with_return

type 'a printer_type = Format.formatter -> 'a -> unit
let install_printer=install_printer
let remove_printer=remove_printer

let refresh=refresh

