include Makefile.inc
-include Makefile.local

mkdebugger:
	@mkdir -p debugger; cd debugger; rm -f *; \
	if [ -f ../patches/$(DEBUGGER_PATCH) ] \
	&& [ -d $(OCAML_SRC_DIR)/debugger ] && (cp $(OCAML_SRC_DIR)/debugger/*.ml* .); \
	then ( \
		rm -f lexer.mli; \
		patch -p0 < ../patches/$(DEBUGGER_PATCH); \
	); \
	else echo "(rule (copy ../patches/ocamldebug+.sh main.bc))" > dune; \
	fi

mktoplevel:
	@mkdir -p toplevel; cd toplevel; rm -f *; \
	if [ -f ../patches/$(TOPLEVEL_PATCH) ] \
	&& [ -d $(OCAML_SRC_DIR)/toplevel ] && (cp $(OCAML_SRC_DIR)/toplevel/*.ml* .); \
	then ( \
		rm -f genprintval.ml* opttop*; \
		patch -p0 < ../patches/$(TOPLEVEL_PATCH); \
	); \
	else echo "(rule (copy ../patches/ocaml+.sh topstart.bc))" > dune; \
	fi

mklib:
	@cd lib; cp genprintval.ml-$(OCAML_VERSION) genprintval.ml

build: mklib mkdebugger mktoplevel
	dune build -p genprint

install:
	dune install
