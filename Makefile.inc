# chop 4.07.1 down to 4.07
OCAML_VERSION:=$(shell opam config var ocaml:version| sed -E 's/\.[0-9]+([-+~].+)*$$//')
# if greater than...
ifeq "$(firstword $(sort $(OCAML_VERSION), 4.14))" "4.14"
OCAML_SRC_DIR:=`opam config var ocaml-variants:build`
else
OCAML_SRC_DIR:=`opam config var ocaml-base-compiler:build`
endif
DEBUGGER_PATCH=debugger-$(OCAML_VERSION).patch
TOPLEVEL_PATCH=toplevel-$(OCAML_VERSION).patch
