Getting Jane Street's Base/Core/Async to print revealed some inadequacies
in Genprintlib - the approach of removing interfaces and signatures 
is crude! Compilation fails sometimes without them and so Genprintlib
leaves such instances and unless the problematic modules in fact expose
their data types in an interface, are left abstracted and unprintable.
So more data will be printed as "<abstr>" than otherwise might be expected.

Two cases are particularly noteworthy.
Core-kernel's Thread_safe_queue is implemented with Obj.magic and exposing
it's innards causes the printer to segfault. So it is special-cased and
left abstract.
Core's Info module is a case that causes a printing stack overflow unless
a step is taken. 
Not having worked out the reason yet, the workaround for printing anything
containing a Info.t is to have this line somewhere early in one's code:
```
[%pr wtf (Core.Info.of_string "?")];
```

In case anyone familiar with compiler internals reads this and might
be able to help, the issue revolves around
running Ctype.moregeneral on the scheme
```
{id=0;level=100000000;desc=Tconstr(int,[],[])}
```
with the Info.t type:
```
{id=1914318;level=100000000;desc=Tconstr(Base__Info.t,[],[])}
```
Maybe the id should be 0? but after the line above is added it's still non-zero
but there is no infinite recursion of Ctype.moregeneral.
I can only speculate that the line above is effecting the Env state in
the linked compiler-libs. Each print statement is worked out in it's own captured Env.t
so what else is shared?
